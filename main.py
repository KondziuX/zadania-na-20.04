#!/usr/bin/env python3

# Task 1
class Arthropod:
    pass


class FaceCream:
    pass


class Cake:
    pass


# Task 2
class Human:
    pass


class Athlete(Human):
    pass


class Footballer(Human):
    pass


# Task 3
# 1
def print_name() -> None:
    print("Konrad")


print_name()


# 2
def number_sum(a: int, b: int, c: int) -> int:
    return a + b + c


print(number_sum(3, 3, 4))


# 3
def print_hello() -> str:
    return "Hello world"


text = print_hello()
print(format(text))


# 4
def difference(a: int, b: int) -> int:
    return a - b


print(difference(3, 2))


# 5
class Car:
    pass


def get_car() -> Car:
    return Car()


print(isinstance(get_car(), Car))
