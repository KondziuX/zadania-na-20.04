#!/usr/bin/env python3

# Task 1
# 1
class Dog:
    def __init__(self):
        self.age: int = 14
        self.name: str = "Dog"


class Poodle(Dog):
    def __init__(self):
        super().__init__()
        print(f"{self.name} is {self.age} years old")


Poodle()


# 2

class Footballer:
    def __init__(self):
        print("I'm a professional footballer")

    def ball(self) -> None:
        print("playing ball")


class Human(Footballer):
    def __init__(self):
        super().__init__()
        print("I'm a Human")


Human()


# 3

class Stadium:
    def __init__(self):
        pass

    def close(self) -> None:
        print("The stadium is closed")


class Stand(Stadium):
    def __init__(self):
        super().__init__()

    def close(self) -> None:
        print("Stand is closed")

    def forced_close(self) -> None:
        super().close()


Stand().close()


# Task 2
# 1

def user_info():
    name: str = input("Name:")
    surname: str = input("Surname:")
    age: str = input("Old:")

    print("You are {} {}. You're {} years old".format(name, surname, age))


user_info()


# 2

def convert():
    number: str = input("Give number:")
    number: int = int(number)

    print("Number after conversion: {}".format(number))


convert()


# 3

class Flower:
    def __init__(self, flower_name: str, flower_color: str) -> str:
        self.flower_name = flower_name
        self.flower_color = flower_color

        print("Your flower's name is {} and is {}".format(self.flower_name, self.flower_color))


Flower("Tulipan", "red")
